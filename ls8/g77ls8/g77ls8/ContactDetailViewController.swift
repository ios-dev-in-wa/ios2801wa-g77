//
//  ContactDetailViewController.swift
//  g77ls8
//
//  Created by WA on 2/25/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ContactDetailViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    func setupWithContact(_ contact: Contact) {
        nameLabel.text = contact.name
        surnameLabel.text = contact.surname
        phoneLabel.text = contact.phone
    }

}
