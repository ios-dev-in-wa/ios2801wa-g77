//
//  ViewController.swift
//  g77ls8
//
//  Created by WA on 2/25/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

//protocol Seasons {
//    func winterCome()
//    func summerCome()
//}
//
//class Rabbit: Seasons {
//
//    var furColor: UIColor = .gray
//
//    func winterCome() {
//        furColor = .white
//    }
//
//    func summerCome() {
//        furColor = .gray
//    }
//}
//
//
//class Bear: Seasons {
//    func winterCome() {
//        sostalLapy()
//    }
//
//    func summerCome() {
//        eat()
//    }
//}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var arrayOfNames: [Contact] = [
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Ihor", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Inna", surname: "Surname", phone: "123123"),
        Contact(name: "Inna", surname: "Surname", phone: "123123"),
        Contact(name: "Dmitro", surname: "Surname", phone: "123123"),
        Contact(name: "Dmitro", surname: "Surname", phone: "123123"),
        Contact(name: "ADmitrortem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
//        Contact(name: "Ihor", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Inna", surname: "Surname", phone: "123123"),
        Contact(name: "Inna", surname: "Surname", phone: "123123"),
        Contact(name: "Dmitro", surname: "Surname", phone: "123123"),
        Contact(name: "Dmitro", surname: "Surname", phone: "123123"),
        Contact(name: "ADmitrortem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123"),
        Contact(name: "Inna", surname: "Surname", phone: "123123"),
        Contact(name: "Inna", surname: "Surname", phone: "123123"),
        Contact(name: "Dmitro", surname: "Surname", phone: "123123"),
        Contact(name: "Dmitro", surname: "Surname", phone: "123123"),
        Contact(name: "ADmitrortem", surname: "Surname", phone: "123123"),
        Contact(name: "Artem", surname: "Surname", phone: "123123")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // TO REMOVE EXTRA SEPARATORS
        tableView.tableFooterView = UIView()
        let nib = UINib(nibName: "ContactTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ContactTableViewCell")

//        let warningView = WarningView(frame: CGRect(x: 0, y: 0, width: 375, height: 150))
        let warningView: WarningView = .fromNib()
        warningView.setupWithDescription("You don't have permission for this!")
        view.addSubview(warningView)
        warningView.center = view.center
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "ShowDetails":
            guard let destinationVC = segue.destination as? ContactDetailViewController, let contact = sender as? Contact else { return }
            _ = destinationVC.view
            destinationVC.setupWithContact(contact)
        case  "ShowAddContact":
            guard let destinationVC = segue.destination as? AddContactViewController else { return }
            destinationVC.onTextSave = { [weak self] contact in
                self?.arrayOfNames.append(contact)
                self?.tableView.reloadSections([0], with: .automatic)
            }
        default: break
        }
    }
    
    @IBAction func editAction(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        sender.title = tableView.isEditing ? "Done" : "Edit"
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath)
//        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
//        cell.textLabel?.text = arrayOfNames[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as? ContactTableViewCell else {
            assertionFailure("asdasds")
            return UITableViewCell()
        }
        let contact = arrayOfNames[indexPath.row]
        // ILLUSTRATION OF REUSE ISSUE
        if contact.name != "Ihor" {
            cell.setupWithText(contact.name)
        } else {
            print("Here is Ihor", indexPath)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            arrayOfNames.remove(at: indexPath.row)
        case .insert:
            let name = arrayOfNames[indexPath.row]
            arrayOfNames.insert(name, at: indexPath.row + 1)
            print("insert")
        case .none: print("NONE")
        @unknown default: print("UNKNOWN")
        }
        tableView.reloadSections([indexPath.section], with: .automatic)
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let element = arrayOfNames[sourceIndexPath.row]
        arrayOfNames.remove(at: sourceIndexPath.row)
        arrayOfNames.insert(element, at: destinationIndexPath.row)
        print(arrayOfNames)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrayOfNames[indexPath.row].name == "Ihor" ? 80 : 40
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contact = arrayOfNames[indexPath.row]
        print("Selected name", contact.name)
        performSegue(withIdentifier: "ShowDetails", sender: contact)
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        switch indexPath.row {
        case 0: return .none
        case 1: return .insert
        default: return .delete
        }
    }

    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let contextItem = UIContextualAction(style: .destructive, title: "DELETE") {  [weak self] (_, _, _) in
            self?.arrayOfNames.remove(at: indexPath.row)
            self?.tableView.reloadSections([indexPath.section], with: .automatic)
            print("DELETE HAPPENS")
        }
        let contextSecondItem = UIContextualAction(style: .normal, title: "Mark") { _, _, _ in
            print("secondItem selected")
        }
        let swipeActions = UISwipeActionsConfiguration(actions: [contextItem, contextSecondItem])
        swipeActions.performsFirstActionWithFullSwipe = false

        return swipeActions
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
