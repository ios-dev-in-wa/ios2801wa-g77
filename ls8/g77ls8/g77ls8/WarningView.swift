//
//  WarningView.swift
//  g77ls8
//
//  Created by WA on 2/25/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class WarningView: UIView {

    @IBOutlet weak var descriptionLabel: UILabel!

//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        backgroundColor = .black
//    }
//    
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    func setupWithDescription(_ text: String) {
        descriptionLabel.text = text
    }
}
