//
//  AddContactViewController.swift
//  g77ls8
//
//  Created by WA on 2/25/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class AddContactViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextfield: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    var onTextSave: ((Contact) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        let warningView: WarningView = .fromNib()
        warningView.setupWithDescription("You don't have permission for this!")
        view.addSubview(warningView)
        warningView.center = view.center
    }

    @IBAction func saveButtonAction(_ sender: UIButton) {
        guard let name = nameTextField.text,
            let surname = surnameTextfield.text,
            let phone = phoneTextField.text,
            !name.isEmpty, !surname.isEmpty else {
            print("YOU NEED TO FILL DATA")
            return
        }
        guard let regex = try? NSRegularExpression(pattern: "\\(?([0-9]{3})\\)?([ .-]?)([0-9]{3})\\2([0-9]{4})", options: []) else { return }
        let range = NSRange(location: 0, length: phone.count)
        guard !regex.matches(in: phone, options: .anchored, range: range).isEmpty else {
            print("PHONE NUMBER REGEX")
            return }
        onTextSave?(Contact(name: name, surname: surname, phone: phone))
        navigationController?.popViewController(animated: true)
    }
}
