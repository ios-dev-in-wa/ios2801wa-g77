//
//  ContactTableViewCell.swift
//  g77ls8
//
//  Created by WA on 2/25/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!

    override func prepareForReuse() {
        nameLabel.text = "HERE IS YOUR NAME"
    }

    func setupWithText(_ text: String) {
        nameLabel.text = text
    }
}
