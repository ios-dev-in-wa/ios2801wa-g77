//
//  Contact.swift
//  g77ls8
//
//  Created by WA on 2/25/20.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct Contact {
    let name: String
    let surname: String
    let phone: String
}
