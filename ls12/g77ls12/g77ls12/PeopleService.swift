//
//  PeopleService.swift
//  g77ls12
//
//  Created by WA on 3/10/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

enum HairColor: String, Decodable {
    case brown, blond, none

    var color: UIColor {
        switch self {
        case .blond: return UIColor.lightGray
        case .brown: return UIColor.brown
        case .none: return UIColor.clear
        }
    }
}

struct HeroResponse: Decodable {

    let results: [StarWarsHero]
}

struct StarWarsHero: Decodable {
    let name: String
    let hairColor: HairColor
    let gender: String
//    let homeworld: String
//    let films, species, vehicles, starships: [String]
//    let created, edited: String
//    let url: String

    enum CodingKeys: String, CodingKey {
        case name
        case hairColor = "hair_color"
        case gender
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        hairColor = (try? container.decode(HairColor.self, forKey: .hairColor)) ?? .none
        gender = try container.decode(String.self, forKey: .gender)
    }
}

struct PeopleService {

    private let session = URLSession.shared

    func getPeople(completion: @escaping (([StarWarsHero]) -> Void)) {
        guard let url = URL(string: "https://swapi.co/api/people/") else { return }
        session.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            switch response.statusCode {
            case 200:
                guard let data = data else { return }
                let decoder = JSONDecoder()
                do {
                    let response = try decoder.decode(HeroResponse.self, from: data)
                    DispatchQueue.main.async {
                        completion(response.results)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            default: break
            }
        }.resume()
    }
}
