//
//  ViewController.swift
//  g77ls12
//
//  Created by WA on 3/10/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private let service = PeopleService()
    private var heroes = [StarWarsHero]()

    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "UserTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "UserTableViewCell")
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        service.getPeople { [weak self] heroes in
            self?.heroes = heroes
            self?.tableView.reloadData()
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return heroes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as? UserTableViewCell else { return UITableViewCell() }
        let hero = heroes[indexPath.row]
        cell.genderLabel.text = hero.gender
        cell.hairColorIndicatorView.backgroundColor = hero.hairColor.color
        cell.nameLabel.text = hero.name
        return cell
    }
}
