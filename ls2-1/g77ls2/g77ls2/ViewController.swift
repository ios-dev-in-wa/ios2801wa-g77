//
//  ViewController.swift
//  g77ls2
//
//  Created by WA on 1/30/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

// CORNER CASE
func doSomeFunnyStuff() {
    print("HAHAHA")
}

class ViewController: UIViewController {

    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        doSomeFunnyStuff()
        // Do any additional setup after loading the view.
        let someConstant: Double = 1
        var someVar = 2
        print(someVar, someConstant)
//        someConstant = 4
        someVar = 5
        print(someVar, someConstant)
        let someText = "asdfasdfadsf"
        print(someText)
        let result = Int(someConstant) / someVar
        print(result)
        sqrt()
        let someVal = multiplier(valueOne: 4, valueTwo: 9)
        greeting()
        intExample()
        let someBool = true
        print(someBool)
        addBox()
    }

    func math() {
        // * / + - %
        let multiply = 2 * 2
        print(multiply)
    }
    

    func sqrt(_ value: Int = 2) {
        let result = value * value
        print(result)
    }

    func multiplier(valueOne: Int, valueTwo: Int) -> Double {
        var newContainer = valueOne
        newContainer = 1
        let result = newContainer * valueTwo
        print(result)
        return Double(result)
    }

    func greeting(name: String = "Artem") {
        print("Hello, ", name, "WOW", name)
    }

    func intExample() {
//        let int = 1
        print(Int.random(in: 4...7))
        print(Double(6).isNaN)
        
    }

    func addBox() {
        let box = UIView()
        box.backgroundColor = .brown
        box.frame = CGRect(x: 50, y: 50, width: 50, height: 50)
        // RADIUS
        box.layer.cornerRadius = 50 / 2
        view.addSubview(box)
        let boxTwo = UIView()
        boxTwo.backgroundColor = .red
        boxTwo.frame = CGRect(x: 50, y: 50, width: 25, height: 25)
        view.addSubview(boxTwo)
    }
}

