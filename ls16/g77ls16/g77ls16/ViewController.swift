//
//  ViewController.swift
//  g77ls16
//
//  Created by WA on 14.04.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let popupView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        popupView.frame = CGRect(x: 100, y: 100, width: 50, height: 50)
        popupView.backgroundColor = .red

        view.addSubview(popupView)

        // Delayed action #1
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
//            self?.popupView.removeFromSuperview()
//        }

        // Delayed action #2
        perform(#selector(removePopup), with: nil, afterDelay: 5)

        // Delayed action #3
//        Timer(fire: Date(), interval: 1, repeats: false) { [weak self] _ in
//            self?.removePopup()
//        }.fire()
    }

    @objc func removePopup() {
        UIView.animate(withDuration: 1, animations: {
            self.popupView.alpha = 0
        }) { _ in
            self.popupView.removeFromSuperview()
            self.popupView.alpha = 1
        }
    }

}

