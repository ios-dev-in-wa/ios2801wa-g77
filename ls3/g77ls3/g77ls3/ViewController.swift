//
//  ViewController.swift
//  g77ls3
//
//  Created by WA on 2/4/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let someText = "Text"


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // < > == !=
        
        let isEqual = 100 != 100
        print(isEqual)
        switch isEqual {
        case true:
            print("is")
        case false:
            print("not")
        }

        foo()
        foo()
        foo()
        foo()
        foo()

        var age = 0
        if isEqual {
            age = 20
        } else {
            age = 40
        }
        let ternarAge = isEqual ? 20 : 40
        print(age, ternarAge)

        isEqual ? foo() : fooString()
    }


    func foo() {
        let moneySize = Int.random(in: 0...2000)
//        if moneySize == 0 {
//            print("You are poor")
//        } else if moneySize == 100 {
//            print("YOu are almost poor")
//        } else if moneySize == 200 {
//            print("YOu are almost poor")
//        }
        switch moneySize {
        case 0:
            print("You are poor", moneySize)
        case 1...100:
             print("YOu are almost poor", moneySize)
        case 101...200:
            print("YOu are almost poor", moneySize)
        case 201...2000:
            print("You are rich", moneySize)
        default:
            print("You are mega rich", moneySize)
        }
    }

    func fooString() {
        let name = "Artem"
        switch name {
        case "Maxim":
            print("Not Artem")
        case "Artem":
            print(name)
        default: break
        }
    }
}

