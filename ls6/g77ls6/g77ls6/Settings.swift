//
//  Settings.swift
//  g77ls6
//
//  Created by WA on 2/18/20.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

final class Settings {

    static let shared = Settings()

    private init() {}

    private let defaults = UserDefaults.standard

    var isSoundOn: Bool {
        get {
            return defaults.bool(forKey: "isSoundOn")
        }
        set {
            defaults.set(newValue, forKey: "isSoundOn")
        }
    }

    var logoColorName: String {
        get {
            return defaults.string(forKey: "logoColorName") ?? "myCompRed"
        }
        set {
            defaults.set(newValue, forKey: "logoColorName")
        }
    }

    func ifFirstLaunchSayHello() {
        guard defaults.value(forKey: "isFirstLaunch") == nil else { return }
        print("HELLO")
        defaults.set("some", forKey: "isFirstLaunch")
    }

    func runCounter() {
        //        let value = UserDefaults.standard.value(forKey: "runCounter") as? Int
        let value = defaults.integer(forKey: "runCounter")
        let newVal = value + 1
        print("Current run count:", newVal)
        defaults.set(newVal, forKey: "runCounter")
    }
}
