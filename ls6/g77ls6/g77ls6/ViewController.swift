//
//  ViewController.swift
//  g77ls6
//
//  Created by WA on 2/18/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    var arrayToSave = [String]()
    let fileUrl = URL(fileURLWithPath: "/Users/wa/Desktop/DataHolder/myArray.plist")
    let fileManager = FileManager.default

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let array = readFileFromDisk(url: fileUrl)
//        print(array)
        writeAsData(array: array, to: "MyArray")
        print(readFromDocuments(fileName: "MyArray"))
//        Settings.shared.isSoundOn = true
        print(Settings.shared.isSoundOn ? "WOW" : "Not WOW")
        view.backgroundColor = UIColor(named: "mainThemeColor")
        imageView.image = UIImage(named: "secondImage")
        let imagesArray = [UIImage(named: "Poof-Frame0"), UIImage(named: "Poof-Frame1"), UIImage(named: "Poof-Frame2"), UIImage(named: "Poof-Frame3"), UIImage(named: "Poof-Frame4")]
        imageView.animationImages = imagesArray.compactMap { $0 }
        imageView.animationDuration = 1
        imageView.animationRepeatCount = 0
        imageView.startAnimating()
    }

    func writeFileToDisk(array: [String], url: URL) {
        let nsArray = arrayToSave as NSArray
        try? nsArray.write(to: url)
    }

    func readFileFromDisk(url: URL) -> [String] {
        guard let fileArray = NSArray(contentsOf: url), let stringArray = fileArray as? [String] else { return [] }
        
        return stringArray
    }

    func writeAsData(array: [String], to fileName: String) {
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
            let fileURL = documentDirectory.appendingPathComponent(fileName)
            let data = try NSKeyedArchiver.archivedData(withRootObject: array, requiringSecureCoding: false)
            try data.write(to: fileURL)
        } catch {
            print(error)
        }
    }

    func readFromDocuments(fileName: String) -> [String] {
        guard let documentDirectory = try? fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false) else { return [] }
        let fileURL = documentDirectory.appendingPathComponent(fileName)
        guard let data = try? Data(contentsOf: fileURL) else { return [] }
        let array = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [String]
        return array ?? []
    }

//    struct StringConstants {
//        static let tokenDefaults = "sksdkfasklmdfkasmdfklakldsgm"
//        static let runCounts = "skmdlfkalsdmfk"//UUID().uuidString
//    }

    func bundleExample() {
//        let urlString = Bundle.main.path(forResource: <#T##String?#>, ofType: <#T##String?#>)
//        let url = URL(string: urlString)
//        Data(contentsOf: url)
    }

//    func goToNextScreen() {
//        performSegue(withIdentifier: "toDetails", sender: ["Strings", "Doubles"])
//    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch segue.identifier {
        case "toDetails":
            let destinationVC = segue.destination as? SecondViewController
//            destinationVC?.delegate = self
            destinationVC?.didSaveText = { [weak self] text in
                self?.textView.text = text
            }
            destinationVC?.labelText = "SUCCESS"
        default: break
        }
    }
}

extension ViewController: SecondViewControllerDelegate {
    func enteredNewText(text: String) {
        textView.text = text
    }
}
