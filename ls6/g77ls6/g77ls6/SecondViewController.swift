//
//  SecondViewController.swift
//  g77ls6
//
//  Created by WA on 2/18/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

protocol SecondViewControllerDelegate: class {
    func enteredNewText(text: String)
}

class SecondViewController: UIViewController {

    @IBOutlet weak var centeredLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    weak var delegate: SecondViewControllerDelegate?
    var labelText: String?
    var didSaveText: ((String) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        let attributedText = NSAttributedString(string: "Hello", attributes: [:])
        centeredLabel.attributedText = attributedText

        let wolf = Wolf()
        let bear = Bear()
        print(wolf is Predator, bear is Predator, wolf is ForestAnimal)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        centeredLabel.text = labelText
    }
    
    @IBAction func saveText(_ sender: UIButton) {
//        let vc = delegate as? ViewController
//        vc?.textView.text = textView.text
        didSaveText?(textView.text)
//        delegate?.enteredNewText(text: textView.text)
    }
}
