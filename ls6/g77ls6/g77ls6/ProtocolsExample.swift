//
//  ProtocolsExample.swift
//  g77ls6
//
//  Created by WA on 2/18/20.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

protocol Predator {
    func attack()
    func eatRabbit()
    var ifKlikiPresnet: Bool { get set }
}

protocol ForestAnimal {
    func hideInDuplo()
}

protocol PeacefulAnimal {
    func eatGrass()
}

class Rabbit: PeacefulAnimal {
    func eatGrass() {
        
    }
}

class Wolf: Predator, ForestAnimal {
    let name: String = "WOLFY"
    func hideInDuplo() {
        print("I m in duplo now")
    }
    
    var ifKlikiPresnet: Bool = false
    
    func attack() {
        
    }
    
    func eatRabbit() {
        
    }
}

class Bear: Predator {
    func attack() {
        
    }
    
    func eatRabbit() {
        
    }
    
    var ifKlikiPresnet: Bool = false
    
    
}
