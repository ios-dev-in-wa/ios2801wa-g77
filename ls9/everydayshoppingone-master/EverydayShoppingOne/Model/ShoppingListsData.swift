//
//  ShoppingListsData.swift
//  EverydayShoppingOne
//
//  Created by V.K. on 2/26/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import Foundation

//class Category {
//    let category: CategoryType
//    let list: List
//}

enum CategoryType: String {
    case groceries = "Groceries"
    case homestuff = "Home stuff"
    case cosmetics = "Cosmetics"
    case drugs = "Drugs"
}

class ShoppingItem {
    let name: String
    var details: String?
//    var restriction: String = "."
    
    init(from item: Item) {
        self.name = item.name
    }
}

class Item {
    let name: String
    let category: CategoryType
    
    init(named: String, to: CategoryType) {
        self.name = named
        self.category = to
    }
}

class List {
    var items: [ShoppingItem] = []
    let category: CategoryType
    let special: Bool
    let name: String = ""
    
    init(regular to: CategoryType) {
        self.category = to
        self.special = false
    }

    init() {
        special = true
        self.category = .cosmetics // temp
    }

//    func foo() {
//        let lists = [List(), List(), List()]
//        let farmacyCat = lists.filter { $0.category == .drugs }
//        if farmacyCat.filter { $0.special == true } != nil {
//            
//        }
//    }
//
//    // My base class func
//    func createListFor(cat: CategoryType) {
//        if myBase.filter { $0.cat == cat && $0.special == true } {
//            // not special list
//        } else {
//            // special
//        }
//    }
}
