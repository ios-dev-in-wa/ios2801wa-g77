//
//  AddShopingListItemView.swift
//  EverydayShoppingOne
//
//  Created by V.K. on 2/27/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class AddShopingListItemView: UIView {
    
    var addShoppingItem: ((String) -> Void)?
    
    @IBAction func addItem(_ sender: UIButton) {
        let newItem = "Tomatos"
        addShoppingItem?(newItem)
    }
    
}
