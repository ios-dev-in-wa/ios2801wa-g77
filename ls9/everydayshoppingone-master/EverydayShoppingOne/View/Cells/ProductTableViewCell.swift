//
//  ProductTableViewCell.swift
//  EverydayShoppingOne
//
//  Created by WA on 2/27/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit
import DropDown

class ProductTableViewCell: UITableViewCell {
    @IBOutlet weak var textFieldContainerView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var infoContainerView: UIView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productDetailLabel: UILabel!

    let dropDown = DropDown()
    private var isInEdit: Bool = false
    var product: ShoppingItem?
//    var dropDownSelection: SelectionClosure?

    override func prepareForReuse() {
        super.prepareForReuse()
        isInEdit = false
        textField.text = nil
        productDetailLabel.isHidden = true
        product = nil
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
        dropDown.anchorView = textField
//            { [weak self] _, value in
//            self?.textField.text = value
//            self?.productDetailLabel?.text = value
//            self?.productDetailLabel.isHidden = false
//            self?.product?.details = value
//            self?.setupEditMode(false)
//        }
//        dropDown.dataSource = ["Apple", "Banana", "Avocado", "Melon"]
    }

    func setupWithProduct(_ product: ShoppingItem, productList: [String], dropDownSelection: SelectionClosure?) {
        self.product = product
        dropDown.dataSource = productList
        productTitleLabel.text = product.name
        dropDown.selectionAction = dropDownSelection
        if let productDetails = product.details {
            productDetailLabel.text = productDetails
            productDetailLabel.isHidden = false
        }
    }

    func setupEditMode(_ isInEdit: Bool) {
        self.isInEdit = isInEdit
        infoContainerView.isHidden = isInEdit
    }
}

extension ProductTableViewCell: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        dropDown.show()
    }
}
