//
//  AppDelegate.swift
//  EverydayShoppingOne
//
//  Created by V.K. on 2/26/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

//    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        let viewController: UIViewController!
//        if UserDefaults.standard.bool(forKey: "didUserLogin") {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            viewController = storyboard.instantiateInitialViewController()
//        } else {
//            let storyboard = UIStoryboard(name: "Prikol", bundle: nil)
//            viewController = storyboard.instantiateInitialViewController()
//            UserDefaults.standard.set(true, forKey: "didUserLogin")
//        }
//
//        window = UIWindow(frame: UIScreen.main.bounds)
//        if let window = self.window {
//               window.rootViewController = viewController
//        }
//        window?.makeKeyAndVisible()

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

