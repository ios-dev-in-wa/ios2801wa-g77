//
//  UIView+Extensions.swift
//  EverydayShoppingOne
//
//  Created by WA on 2/27/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
