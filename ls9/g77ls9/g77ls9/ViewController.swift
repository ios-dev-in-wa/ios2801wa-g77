//
//  ViewController.swift
//  g77ls9
//
//  Created by WA on 2/27/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var greenView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidLayoutSubviews() {
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("GreenView frame", greenView.frame, "\nGreenView bounds", greenView.bounds)
//        greenView.center = view.center // TEMP for constraints
        print(view.frame.intersects(greenView.frame))
        print(view.frame.contains(CGPoint(x: -10, y: -100)))
        let drawView = DrawView(frame: .init(x: 30, y: 30, width: 200, height: 200))
        view.addSubview(drawView)
    }

}

