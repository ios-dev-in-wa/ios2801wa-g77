//
//  DrawView.swift
//  g77ls9
//
//  Created by WA on 2/27/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class DrawView: UIView {
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        let path = UIBezierPath(rect: rect)
        path.move(to: CGPoint(x: 10, y: 10))
        path.addLine(to: CGPoint(x: 40, y: 50))
        path.addLine(to: CGPoint(x: 10, y: 50))
        path.addLine(to: CGPoint(x: 10, y: 10))

        path.usesEvenOddFillRule = true
        UIColor.red.setStroke()
        path.stroke()
        UIColor.green.setFill()
        path.fill()
    }
}
