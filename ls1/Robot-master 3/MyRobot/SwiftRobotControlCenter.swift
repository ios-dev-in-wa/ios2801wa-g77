//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//
import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L2C" //  Level name
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        for _ in 0...7 {
            findwall()
            upwall()
            obhod()
        }
    }
    
    func findwall(){
        while frontIsClear {
            move()
        }
        if frontIsBlocked {
            turnRight()
        }
    }
    
    func upwall(){
        while !leftIsClear {
            move()
        }
    }
    
    func obhod() {
        turnLeft()
        move()
        turnLeft()
        findwall()
    }
    
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
}
