//
//  OnboardingModel.swift
//  g77ls14
//
//  Created by WA on 07.04.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct OnboardingModel {
    let imageName: String
    let title: String
    let subtitle: String
}
