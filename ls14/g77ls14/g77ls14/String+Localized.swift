//
//  String+Localized.swift
//  g77ls14
//
//  Created by WA on 07.04.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

extension String {

    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
