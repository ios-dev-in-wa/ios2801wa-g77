//
//  ViewController.swift
//  g77ls14
//
//  Created by WA on 07.04.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

protocol OnboardingDelegate: class {
    func openPage(with index: Int)
}

class ViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var skipTutorialButton: UIButton!

    weak var delegate: OnboardingDelegate?

    let onboardingModels: [OnboardingModel] = [
        OnboardingModel(imageName: "image1", title: "Hey there!", subtitle: "onboardingFirstStepSubtitle"),
        OnboardingModel(imageName: "image2", title: "Do you know ..?", subtitle: "We solve it...!"),
        OnboardingModel(imageName: "image3", title: "Just try! DO IT!", subtitle: "We are cool")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.numberOfPages = onboardingModels.count
        skipTutorialButton.setTitle("Skip tutorial".localized, for: .normal)
        print(Locale.isoLanguageCodes)
        print(Locale.current.regionCode, Locale.current.languageCode)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "ToPageVC":
            guard let controller = segue.destination as? OnboardingPageViewController else { return }
            delegate = controller
            controller.onboardingDelegate = self
            let controllers = onboardingModels.compactMap { model -> UIViewController? in
                guard let controller = storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as? OnboardingViewController else { return nil }
                _ = controller.view
                controller.setupWith(model: model)
                return controller
            }
            controller.setControllers(controllers: controllers)
        default: break
        }
    }

    @IBAction func skipTutorialAction(_ sender: UIButton) {

    }

    @IBAction func pageControllerAction(_ sender: UIPageControl) {
        delegate?.openPage(with: sender.currentPage)
    }

    private func setLocalizations() {
        
    }
}

extension ViewController: OnboardingControllerDelegate {
    func indexIsChanged(index: Int) {
        pageControl.currentPage = index
    }
}
