//
//  OboardingViewController.swift
//  g77ls14
//
//  Created by WA on 07.04.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

    func setupWith(model: OnboardingModel) {
        imageView.image = UIImage(named: model.imageName.localized)
        titleLabel.text = model.title.localized
        subtitleLabel.text = model.subtitle.localized
    }
}
