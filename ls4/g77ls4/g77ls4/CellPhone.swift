//
//  CellPhone.swift
//  g77ls4
//
//  Created by WA on 2/6/20.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class CellPhone {
    var manufacturer :String
    private let model :String
    let price :String
    let sensorDisplay :Bool
    let camera: Bool
    let flashlight :Bool
    let simCard :Bool
    
    init(manufacturer: String, model: String, price: String, sensorDisplay: Bool, camera: Bool, flashlight: Bool, simCard: Bool) {
        self.manufacturer = manufacturer
        self.model = model
        self.price = price
        self.sensorDisplay = sensorDisplay
        self.camera = camera
        self.flashlight = flashlight
        self.simCard = simCard
    }

    private init(phone: CellPhone) {
        self.manufacturer = phone.manufacturer
        self.model = phone.model
        self.price = phone.price
        self.sensorDisplay = phone.sensorDisplay
        self.camera = phone.camera
        self.flashlight = phone.flashlight
        self.simCard = phone.simCard
        sendLog()
    }

    private func sendLog() {
        
    }
    
    func ring() {
        print("DING DONG")
        sendLog()
    }
}

class SmarthPhone: CellPhone {

    private func sendLog() {
        
    }

    override func ring() {
        
    }
}

struct CellPhoneStruct {
    var manufacturer :String
    let model :String
    let price :String
    let sensorDisplay :Bool
    let camera: Bool
    let flashlight :Bool
    let simCard :Bool
}


final class LocalStorage {

    var storageLimit: Int = 1000

    private init() { }

    static let shared = LocalStorage()

    func store() {
        
    }
}
