//
//  MapViewController.swift
//  g77ls15
//
//  Created by WA on 09.04.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!

    let manager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()

        mapView.showsUserLocation = true

        manager.delegate = self
        // Do any additional setup after loading the view.
    }
}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
    }
}
