//
//  ImageDownloadManager.swift
//  g77ls15
//
//  Created by WA on 09.04.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ImageDownloadManager {

    static let shared = ImageDownloadManager()

    private init() { }

    private let cache = NSCache<NSString, AnyObject>()

    private func downloadImage(path: String, completion: ((UIImage) -> Void)?) {
        guard let url = URL(string: path) else { return }
        URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            self?.cache.setObject(image, forKey: path as NSString)
            DispatchQueue.main.async {
                completion?(image)
            }
        }.resume()
    }

    func getImage(for path: String, completion: ((UIImage?) -> Void)?) {
        guard let url = URL(string: path) else { return }
        if let cachedImage = cache.object(forKey: url.absoluteString as NSString) as? UIImage {
            completion?(cachedImage)
        } else {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data: data)
                    self.cache.setObject(imageToCache!, forKey: url.absoluteString as NSString)
                    completion?(imageToCache)
                }
            }.resume()
        }
//        if let image = cache.object(forKey: path as NSString) as? UIImage {
//            completion?(image)
//        } else {
//            getImage(for: path, completion: completion)
//        }
    }
}
