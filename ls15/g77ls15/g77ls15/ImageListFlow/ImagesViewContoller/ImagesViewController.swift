//
//  ViewController.swift
//  g77ls15
//
//  Created by WA on 09.04.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ImagesViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    private let imageNames = ["https://www.5.ua/media/pictures/original/166028.jpg", "https://i.insider.com/5e21d997ab49fd093647a4c6?width=1100&format=jpeg&auto=webp", "https://sjferret.com/wp-content/uploads/Thinking-of-getting-a-cat.png", "https://sjferret.com/wp-content/uploads/Thinking-of-getting-a-cat.png", "https://sjferret.com/wp-content/uploads/Thinking-of-getting-a-cat.png", "https://sjferret.com/wp-content/uploads/Thinking-of-getting-a-cat.png"]

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
    }


    @IBAction func showSettingsAction(_ sender: UIBarButtonItem) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
//        guard let settingsUrl = URL(string: "tel://+380503111111") else { return }
 
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
}

extension ImagesViewController: UICollectionViewDelegate {
    
}

extension ImagesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell else {
            fatalError("ImageCollectionViewCell doesn't exist")
        }
        cell.setupWith(imageName: imageNames[indexPath.row])
        return cell
    }
    
    
}
