//
//  ImageCollectionViewCell.swift
//  g77ls15
//
//  Created by WA on 09.04.2020.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Kingfisher

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageNameLabel: UILabel!
    
    func setupWith(imageName: String) {
//        ImageDownloadManager.shared.getImage(for: imageName) { [weak self] image in
//            self?.imageView.image = image
//        }
        imageView.kf.indicatorType = .activity
        if let url = URL(string: imageName) {
            imageView.kf.setImage(with: url)
        }
        imageNameLabel.text = imageName
    }
}
