//
//  Car.swift
//  g77ls3-2
//
//  Created by WA on 2/4/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class Vehice {
    let kolesa: Int
    let isBaranka: Bool

    init(kolesa: Int, isBaranka: Bool) {
        self.kolesa = kolesa
        self.isBaranka = isBaranka
    }

    func run() {
        print("I am running on my legs")
    }
}

class Car: Vehice {
    let color: UIColor

    init(color: UIColor, kolesa: Int, isBaranka: Bool) {
        self.color = color
        super.init(kolesa: kolesa, isBaranka: isBaranka)
    }

    override func run() {
        super.run()
        print("I ma driving my dream")
    }
}
