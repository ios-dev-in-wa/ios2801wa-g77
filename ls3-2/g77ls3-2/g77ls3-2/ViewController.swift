//
//  ViewController.swift
//  g77ls3-2
//
//  Created by WA on 2/4/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var completion: ((String) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        let string = "Name Artem, name Valera, name Inna"
//
//        let reversed: [Character] = string.reversed()
//        print(reversed)
//
//        let test = string.replacingOccurrences(of: "name", with: "haha")
//        print(test)
//
////        print(string.lowercased())
////        print(string.uppercased())
////        print(string.)
//        var users: [String] = [
//            "Artem", "Ihor", "Victor"
//        ]
//        greetingsFor(users: users)
//        print(users[0])
//        users.removeAll()
//        let maybeValera: String? = "Valera"
//        let nina = users.first ?? maybeValera ?? "NIna"
//        print(users.first ?? "NIna")
//        notNilExample(users: users)
//        completion = {
//            print("HELLO")
//        }
//        completion?()
//        completion
            
////            greetingsFor(users: ["Artem", "Ihor"])
////        filterSortExample(users: ["Artem", "Anton", "Inga", "IhorA"])
//        let val = users.first { name -> Bool in
//            return name.contains("I")
//        }
//
//        let index = users.firstIndex(of: "Artem")
//        print(users[index ?? 0])
//        users += ["Hey"]
//        users.append("WOw")
////        users.insert("Heyyy", at: 100)
//        print(users)
//
////        let anyArray = [Any]()

//        let set: Set<Int> = [1,2,3,4,5]
//        dictionaryExample()

        let redCar = Car(color: .red, kolesa: 4, isBaranka:  false)
        print(redCar.kolesa)
        redCar.run()
    }

    func greetingsFor(users: [String]) {
        users.forEach { name in
            print("Hello,", name)
        }
        
//        users.forEach { print("Hello,", $0) }
//        for name in users {
//            print("Hello,", name)
//        }
    }

    func notNilExample(users: [String]) {
        if let firstElement = users.first {
            print(firstElement)
        } else {
            print("No first element")
        }
    }

    func filterSortExample(users: [String]) {
        let filtered = users.filter { value -> Bool in
            return value.contains("A")
        }
        users.filter { $0.contains("A") }
        print(filtered)

//        let sorted = users.sorted(by: <)
//        print(sorted)
    }

    func optionalParam(name: String?, count: Int) {
        if let newName = name {
            print("SUCCESS", newName)
        } else {
            print("ERROR")
        }
        for _ in 0...count {
            print("hahah")
        }
    }

    func dictionaryExample() {
        var phoneBook: [String: String] = [
            "Police": "102",
            "Emergency": "103",
            "James BOnd": "007"
        ]
        phoneBook["Police"] = "1020"
//        phoneBook["Emergency"] = "103"
//        phoneBook["GasSlyzhba"] = "104"

        if let phoneNumber = phoneBook["Police"] {
            print(phoneNumber)
        }
//        let realString: String = phoneBook["Emergency"]
        print(phoneBook.keys)
        print(phoneBook.values)

        var modernPhoneBook: [String: [String]] = [
            "Police": ["102", "911"],
            "Emergency": ["103", "911"],
            "James BOnd": ["007", "700"]
        ]
        
        print(modernPhoneBook["Police"])
//        let some = modernPhoneBook.values
//        print(some)
//        print(phoneBook.index(forKey: "Police"))
    }
}

