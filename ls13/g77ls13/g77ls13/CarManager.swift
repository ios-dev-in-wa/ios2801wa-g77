//
//  CarManager.swift
//  g77ls13
//
//  Created by WA on 3/12/20.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation
import Parse

final class CarManager {

    static let shared = CarManager()

    private init() {}

    private let query = PFQuery(className:"Car")

    func fetchCars(completion: @escaping (([PFObject]) -> Void)) {
        query.findObjectsInBackground { results, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let results = results else { return }
            completion(results)
        }
    }

    func createCar(carToSave: Car, completion: ((Bool) -> Void)?) {
        let object = PFObject.init(className: "Car")
        object["name"] = carToSave.name
        object["productionYear"] = carToSave.productionYear
        object["wasInAccident"] = carToSave.wasInAccident
        object["owner"] = PFUser.current()
        let relation = object.relation(forKey: "ownerRelation")
        relation.add(PFUser.current()!)

        object.saveInBackground { isFinished, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            completion?(isFinished)
        }
    }
}
