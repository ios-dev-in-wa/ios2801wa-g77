//
//  AddCarViewController.swift
//  g77ls13
//
//  Created by WA on 3/12/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Parse

struct Car: Encodable {
    let objectId: String
    let name: String
    let productionYear: String
    let wasInAccident: Bool
}

class AddCarViewController: UIViewController {
    
    struct Constants {
        static let carName = "Car name"
    }

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var productionYearTextField: UITextField!
    @IBOutlet weak var wasInAccidentSwitch: UISwitch!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    private var yearPicker: UIPickerView?

    var yearsTillNow: [String] {
        var years = [String]()
        let year = Calendar.current.component(.year, from: Date())
        for i in (1970..<year).reversed() {
            years.append("\(i)")
        }
        return years
    }
    
    @IBAction func saveAction(_ sender: UIBarButtonItem) {
        guard let name = nameTextField.text, !name.isEmpty,
            let productionYear = productionYearTextField.text, !productionYear.isEmpty else { return }
        activityIndicator.startAnimating()
//        let car = Car(name: name, productionYear: productionYear, wasInAccident: wasInAccidentSwitch.isOn)
        CarManager.shared.createCar(carToSave: car) { [weak self] isFinished in
            self?.activityIndicator.stopAnimating()
            guard isFinished else { return }
            DispatchQueue.main.async {
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func prodcutionYearTapAction(_ sender: UITapGestureRecognizer) {
        productionYearTextField.resignFirstResponder()
        let yearPicker = UIPickerView()
        view.addSubview(yearPicker)
        yearPicker.translatesAutoresizingMaskIntoConstraints = false

        yearPicker.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        yearPicker.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        yearPicker.widthAnchor.constraint(equalToConstant: 400).isActive = true
        yearPicker.heightAnchor.constraint(equalToConstant: 400).isActive = true

        yearPicker.dataSource = self
        yearPicker.delegate = self
        self.yearPicker = yearPicker
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        yearPicker?.removeFromSuperview()
    }
}

extension AddCarViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearsTillNow.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return yearsTillNow[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        productionYearTextField.text = yearsTillNow[row]
    }
}
