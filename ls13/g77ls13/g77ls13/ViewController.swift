//
//  ViewController.swift
//  g77ls13
//
//  Created by WA on 3/12/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController()

    var objects = [Car]()
    var filteredCars = [Car]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self

        searchController.searchResultsUpdater = self
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = "Search Candies"
        // 4
        navigationItem.searchController = searchController
        // 5
        definesPresentationContext = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CarManager.shared.fetchCars { [weak self] objects in
            self?.objects = objects.compactMap { object in
                guard let name = object["name"] as? String,
                    let year = object["productionYear"] as? String,
                    let wasInAccident = object["wasInAccident"] as? Bool,
                    let id = object.objectId else { return nil }
                return Car(objectId: id, name: name, productionYear: year, wasInAccident: wasInAccident)
            }
            self?.tableView.reloadData()
        }
    }


    @IBAction func logoutAction(_ sender: UIBarButtonItem) {
        PFUser.logOut()
        let mySceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate
        mySceneDelegate?.showLogin()
    }

    @IBAction func showSearchAction(_ sender: UIBarButtonItem) {
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarCell", for: indexPath)
        cell.textLabel?.text = objects[indexPath.row].name
        cell.detailTextLabel?.text = objects[indexPath.row].productionYear
        return cell
    }
}

extension ViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { [weak self] _, _, _ in
//            guard let object = self?.objects[indexPath.row] else { return }
//            object.deleteInBackground { isFinished, error in
//                if let error = error {
//                    print(error.localizedDescription)
//                }
//                guard isFinished else { return }
//                self?.objects.remove(at: indexPath.row)
//                self?.tableView.reloadSections([0], with: .automatic)
//            }
//        }
//        return UISwipeActionsConfiguration(actions: [deleteAction])
//    }
}

extension ViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    // TODO
  }
}
