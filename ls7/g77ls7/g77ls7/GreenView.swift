//
//  GreenView.swift
//  g77ls7
//
//  Created by WA on 2/20/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class GreenView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .green
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
