//
//  ViewController.swift
//  g77ls7
//
//  Created by WA on 2/20/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let greenView: UIView = GreenView()
    let imageView = UIImageView()

    override func loadView() {
        super.loadView()
        view = greenView
        print("loadView")
        imageView.image = UIImage(named: "asdasdas")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        let contactOne = Contact(name: "Artem", surname: "Velykyy")
        let contactTwo = Contact(name: "Ihor", surname: "Rombikov")
        if contactOne == contactTwo {
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(secondViewControllerDidAppear), name: .secondViewControllerDidAppear, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
    }

    @objc func secondViewControllerDidAppear(notification: Notification) {
        print(notification.object)
        print("secondViewControllerDidAppear")
    }
}

