//
//  ErrorViewController.swift
//  g77ls7
//
//  Created by WA on 2/20/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class ErrorViewController: UIViewController {

    @IBOutlet weak var errorLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func errorAction(_ sender: UIButton) {
        print(sender.currentTitle, sender.tag)
        let alertVC = UIAlertController(title: "ERROR", message: "We can't load image \(sender.tag)", preferredStyle: .actionSheet)
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: { [weak self] _ in
            self?.errorLabel.text = "TRY AGAIN"
            print("DID PRESS CANCEL")
        })
        alertVC.addAction(okAction)
        
        present(alertVC, animated: true, completion: nil)
    }
}
