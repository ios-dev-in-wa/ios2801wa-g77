//
//  SecondViewController.swift
//  g77ls7
//
//  Created by WA on 2/20/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let secondViewControllerDidAppear: Notification.Name = Notification.Name(rawValue: "secondViewControllerDidAppear")
}

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.post(name: .secondViewControllerDidAppear, object: "DID APPEAR")
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let desctinationVC = segue.destination as? ErrorViewController, let errorMsg = sender as? String else { return }
        _ = desctinationVC.view
        desctinationVC.errorLabel.text = errorMsg
    }

    @objc func orientationDidChange() {
        print("orientationDidChange")
    }

    @IBAction func goBack(_ sender: UIButton) {
        performSegue(withIdentifier: "showError", sender: [1, 4, 6])
//        guard let vc = storyboard?.instantiateViewController(withIdentifier: "ErrorViewController") as? ErrorViewController else { return }
//        _ = vc.view // force view load
//        vc.errorLabel.text = "OUCH try again"
        // present modally
//        vc.modalPresentationStyle = .fullScreen
//        vc.modalTransitionStyle = .flipHorizontal
//        present(vc, animated: true, completion: nil)

//        navigationController?.pushViewController(vc, animated: true)
        // Only for modal screens
//        dismiss(animated: true, completion: nil)
//        navigationController?.popViewController(animated: true)
//        navigationController?.popToRootViewController(animated: true)
        
    }
}
