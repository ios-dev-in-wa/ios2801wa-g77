//
//  Contact.swift
//  g77ls7
//
//  Created by WA on 2/20/20.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class Contact: Equatable {
    let name: String
    let surname: String

    init(name: String, surname: String) {
        self.name = name
        self.surname = surname
    }
    
//    func compareWith(contact: Contact) -> Bool {
//       return true
//    }

    static func == (lhs: Contact, rhs: Contact) -> Bool {
        return lhs.name == rhs.name && lhs.surname == rhs.surname 
    }
}
