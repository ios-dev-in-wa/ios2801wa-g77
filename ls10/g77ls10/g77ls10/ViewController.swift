//
//  ViewController.swift
//  g77ls10
//
//  Created by WA on 3/3/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import AVKit

struct Answer { }

struct Graph {
    let question: String
    let answers: [Graph]
}

class ViewController: UIViewController {

    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet weak var exampleView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var exmpleViewCenterXConstraint: NSLayoutConstraint!

    var player: AVPlayer?
    let someCloshure: ((Bool) -> Int) = { isFinished in
        let someText = "WOW"
        print(isFinished ? someText :  "OMG")
        return Int.max
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        let currentDate = Date()
//        print(currentDate.timeIntervalSince1970)
//        print(currentDate)
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm:ss.SSS"
//        print(dateFormatter.string(from: currentDate))
//        let components = Calendar.current.dateComponents([.hour, .weekday], from: currentDate)
//        print(components)
////        dateFormatter.timeZone = TimeZone(abbreviation: <#T##String#>)
//        print(dateFormatter.date(from: "00:14:21"))
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction))
        exampleView.addGestureRecognizer(panGesture)
        print(someCloshure(false))
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.blue.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)

//        progressView
        exampleView.layer.cornerRadius = 20
        exampleView.layer.shadowColor = UIColor.black.cgColor
        exampleView.layer.shadowOffset = .init(width: 5, height: 5)
        exampleView.layer.shadowOpacity = 0.5

        let result = min(0, 2)
        let height = Int.random(in: 0...500)
        let resultTwo = min(200, height)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard exmpleViewCenterXConstraint.constant != 0 else { return }
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: {
            self.exmpleViewCenterXConstraint.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
        UIView.animate(withDuration: 1, delay: 0, options: [.repeat, .curveLinear], animations: {
            self.imageView.transform = .init(rotationAngle: CGFloat.pi)
        }, completion: nil)
    }

    @IBAction func swipeAction(_ sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case .down:
            print("")
        case .left, .right, .up:
            switch sender.state {
            case .possible:
                print("POSSIBLE swipe")
            case .began:
                print(sender.location(in: view))
                print("BEGAN swipe")
            case .changed:
//                let locationPoint = sender.location(in: view)
//                sender.view?.center = locationPoint
//                print(sender.translation(in: view))
//                print(sender.velocity(in: view))
                        print("CHANGED swipe")
            case .ended:
                print("ENDED swipe")
            case .cancelled:
                print("CANCELLED swipe")
            case .failed:
                print("FAILED swipe")
            @unknown default: break
            }
        default: break
        }
    }
    
    @objc func panGestureAction(sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .possible:
            print("POSSIBLE PAN")
        case .began:
            print(sender.location(in: view))
            print("BEGAN PAN")
        case .changed:
            let locationPoint = sender.location(in: view)
            sender.view?.center = locationPoint
            print(sender.translation(in: view))
            print(sender.velocity(in: view))
            print("---------------------")
//            print("CHANGED PAN")
        case .ended:
            print("ENDED PAN")
        case .cancelled:
            print("CANCELLED PAN")
        case .failed:
            print("FAILED PAN")
        @unknown default: break
        }
    }

    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        print("TAPPED")
        if let url = Bundle.main.url(forResource: "00182", withExtension: "mp3") {
//                let data = try Data(contentsOf: url)
            player = AVPlayer(url: url)
            player?.play()
        }
//        UIView.animate(withDuration: 0.5) {
////            self.exampleView.backgroundColor = .random
//            //            self.exampleView.alpha = CGFloat.random(in: 0...1)
//            self.exampleView.frame.origin = CGPoint(x: CGFloat.random(in: 0...self.view.frame.maxX),
//                                              y: CGFloat.random(in: 0...self.view.frame.maxY))
////            self.exampleView.frame.size.height = CGFloat.random(in: 100...600)
//        }
//        UIView.animate(withDuration: 1, delay: 0, options: [.curveEaseOut, .], animations: {
////            self.exampleView.backgroundColor = .random
////            self.exampleView.alpha = CGFloat.random(in: 0...1)
//            self.exampleView.frame.origin = CGPoint(x: CGFloat.random(in: 0...self.view.frame.maxX),
//                                                         y: CGFloat.random(in: 0...self.view.frame.maxY))
//        }) { isFinished in
//            print(isFinished)
//        }

//        UIView.transition(with: exampleView, duration: 0.5, options: [.transitionFlipFromLeft], animations: {
//            self.exampleView.backgroundColor = .random
//        }) { _ in
//            print("sdfsdf")
//        }
//        UIView.transition(from: exampleView, to: view, duration: 0.5, options: [.transitionFlipFromTop]) { _ in
//            print("transitioned")
//        }
        switch sender {
        case tapGesture:
            switch sender.state {
            case .possible:
                print("POSSIBLE TAP")
            case .began:
                print("BEGAN TAP")
            case .changed:
                print("BEGAN TAP")
            case .ended:
                print("ENDED TAP")
            case .cancelled:
                print("CANCELLED TAP")
            case .failed:
                print("FAILED TAP")
            @unknown default: break
            }
        default: break
        }
    }
    
    @IBAction func pinchAction(_ sender: UIPinchGestureRecognizer) {
        print(sender.scale)
        switch sender.state{
        case .changed:
            sender.view?.transform = CGAffineTransform(scaleX: sender.scale, y: sender.scale)
        case .ended:
            sender.view?.transform = .identity
        default: break
        }
    }

    @IBAction func rotationAction(_ sender: UIRotationGestureRecognizer) {
        print(sender.rotation)
        sender.view?.transform = .init(rotationAngle: sender.rotation)
    }

    @IBAction func longTapAction(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .possible:
            print("POSSIBLE longTap")
        case .began:
            print("BEGAN longTap")
        case .changed:
            print("Changed longTap")
        case .ended:
            print("ENDED longTap")
        case .cancelled:
            print("CANCELLED longTap")
        case .failed:
            print("FAILED longTap")
        @unknown default: break
        }
    }

    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
       // code you want to implement
        print(motion)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
