//
//  ViewController.swift
//  IvanVasilievich
//
//  Created by maxxx on 07.02.2020.
//  Copyright © 2020 maxxx. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {

    typealias ErrorClosure = ((Error) -> Void)

    var errorClosure: ErrorClosure = { error in
        print(error.localizedDescription)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        do {
            try foo(sendedString: "")
        } catch {
            let error = error as? EmptyError
            print(error?.localizedDescription)
        }
        let robot = Robot(name: "C3-P0")
        print(robot)
    }
 
    //1
    func charCount(name:String) -> Int {
        return name.count
    }
    
    //2
    func iChNa(){
        if let sex = detectSexBy(otchestvo: "Михайлович"){
            print(sex)
        } else {
            print("Что то пошло нетак!Проверьте отчество и попробуйте еще раз")
        }
        
    }
    
    func detectSexBy(otchestvo: String) -> String? {
        let sex: String?

        if otchestvo.lowercased().hasSuffix("ич") {
            sex = "Man"
        } else if otchestvo.lowercased().hasSuffix("на") {
            sex = "Woman"
        } else {
            sex = nil
        }

        return sex
    }
    
    //3
    class EmptyError: Error {
        var errorText = "WOW! This string is empty"
        var localizedDescription: String {
            return "WOW! This string is empty"
        }
    }

    func foo(sendedString: String) throws {
        if sendedString.isEmpty {
            throw EmptyError()
        }
        if sendedString.contains("WOW") {
            throw EmptyError()
        }
    }

    func splitLine(string: String){
        do {
            let regexp: NSRegularExpression = try NSRegularExpression.init(pattern: "([a-z])([A-Z])", options: [])
            let range = NSMakeRange(0, string.count)
            let newString = regexp.stringByReplacingMatches(in: string, options: [], range: range, withTemplate: "$1 $2")
            let array: Array = newString.components(separatedBy: " ")
            print(array.joined(separator: " "))
        } catch {
            let error = error as? EmptyError
            print(error?.errorText)
        }
    }
    
    func splitByUpCase(string: String) -> String {
        var result = ""

        for ch in string {
            if String(ch) == String(ch).uppercased() {
                result.append(" ")
            }

            result.append(ch)
        }

        return result
    }
    
    //4
    func reverseOwn(string: String) -> String {
        var result: String = ""

        for ch in string {
            result.insert(ch, at: result.startIndex)
        }

        return result
    }
    
    //5
    func formatLikeCalculator(number: String) -> String {
        var result = ""

        for (index, element) in number.reversed().enumerated() {
            result.insert(element, at: result.startIndex)

            if ((index + 1) % 3 == 0 && index != number.count - 1) {
                result.insert(",", at: result.startIndex)
            }
        }

        return result
    }
    
    //6
    func checkSecurePassword(password: String) -> Int{
        var secureLevel = 0

        if containsNumber(password) {
            secureLevel += 1
        }

        if  containsLowcaseChar(password){
            secureLevel += 1
        }

        if  containsUpercaseChar(password){
            secureLevel += 1
        }

        if  containsSpecialChar(password){
            secureLevel += 1
        }

        if  containsSpecialChar(password) && containsUpercaseChar(password) &&
            containsLowcaseChar(password) && containsNumber(password){
            secureLevel += 1
        }

        return secureLevel
    }
    
    func containsSpecialChar(_ string: String) -> Bool {
        return string.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil
    }

    func containsUpercaseChar(_ string: String) -> Bool {
        return string.rangeOfCharacter(from: CharacterSet.uppercaseLetters) != nil
    }

    func containsLowcaseChar(_ string: String) -> Bool {
        return string.rangeOfCharacter(from: CharacterSet.lowercaseLetters) != nil
    }

    func containsNumber(_ string: String) -> Bool {
        let numbersRange = string.rangeOfCharacter(from: .decimalDigits)
        return (numbersRange != nil)
    }

    //7
    func sortAndDelCopies(_ array: [Int]) -> [Int] {
        var result = array

        for x in 1..<result.count {
            var y = x
            while y > 0 && result[y] < result[y - 1] {
                result.swapAt(y - 1, y)
                y -= 1
            }
        }
        return result
    }
    
    func deleteDublicats(array: [Int]) -> [Int] {
        return Array(Set(array))
    }
    
    //8
    
    
}

