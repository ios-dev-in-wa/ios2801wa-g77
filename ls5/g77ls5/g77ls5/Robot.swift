//
//  Robot.swift
//  g77ls5
//
//  Created by WA on 2/11/20.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

class Robot: NSObject {
    let name: String
    var health: Int = 100
    let americano = Drink.americano

    init(name: String) {
        self.name = name
    }

    override var description: String {
//        drink.coffeeBeansNeeded
        return "\(name), health: \(health)"
    }

    var isBigGun: Bool = false

    var drink = Drink.latte
}


enum Drink {
    case americano, latte

    var coffeeBeansNeeded: Int {
        switch self {
        case .americano: return 20
        case .latte: return 30
        }
    }
}
