//
//  MainViewController.swift
//  g77ls5
//
//  Created by WA on 2/11/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var centeredButton: UIButton!

//    let myCoffeMachine = COffeMachine()

    override func viewDidLoad() {
        super.viewDidLoad()
//        view
        print("COOOL")
        statusLabel.text = "HELLLO"
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        centeredButton.setTitle("HELLo", for: .normal)
        centeredButton.setTitle("BYE", for: .highlighted)
//        view.backgroundColor = .black
    }
    
    @IBAction func centeredAction(_ sender: UIButton) {
        print(sender.currentTitle)
//        myCoffeMachine.addMilk()
    }
}
