//
//  DetailViewController.swift
//  g77ls11
//
//  Created by WA on 3/5/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

protocol DetailViewControllerDelegate: class {
    func didSelectImage(_ image: UIImage)
}

class DetailViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var detailDescriptionLabel: UILabel!

    weak var delegate: DetailViewControllerDelegate?

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.description
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch segue.identifier {
        case "showProfileHeaderView":
            guard let destinationVC = segue.destination as? ProfileHeaderViewController else { return }
            delegate = destinationVC
//            destinationVC.delegate = self
            destinationVC.onImageTap = { [weak self] in
                self?.showPhotoLibrary()
            }
        default: break
        }
    }

    var detailItem: NSDate? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    func showPhotoLibrary() {
        let photoLibraryVC = UIImagePickerController()
        photoLibraryVC.delegate = self
        present(photoLibraryVC, animated: true, completion: nil)
    }

}

extension DetailViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        delegate?.didSelectImage(image)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

