//
//  ProfileHeaderViewController.swift
//  g77ls11
//
//  Created by WA on 3/5/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit

protocol ProfileHeaderViewControllerDelegate: class {
    func didPressOnImage()
}

class ProfileHeaderViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!

    var onImageTap: (() -> Void)?
    weak var delegate: ProfileHeaderViewControllerDelegate?

    private var tapGesture: UITapGestureRecognizer?

    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didPressOnImageView))
        self.tapGesture = tapGesture
        profileImageView.addGestureRecognizer(tapGesture)
    }

    func setupWithUser(_ user: User) {
        profileImageView.image = UIImage(named: user.imageName)
        profileNameLabel.text = user.name
    }

    @objc private func didPressOnImageView() {
        onImageTap?()
    }
}

extension ProfileHeaderViewController: DetailViewControllerDelegate {
    func didSelectImage(_ image: UIImage) {
        profileImageView.image = image
    }
}
