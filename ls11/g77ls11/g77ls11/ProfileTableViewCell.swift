//
//  ProfileTableViewCell.swift
//  g77ls11
//
//  Created by WA on 3/5/20.
//  Copyright © 2020 WA. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!

    func setupWithUser(_ user: Result) {
        profileImageView.kf.indicatorType = .activity
        if let url = URL(string: user.picture.large) {
            profileImageView.kf.setImage(with: url)
        }
        nameLabel.text = user.name.fullName
        genderLabel.text = user.gender.rawValue
        emailLabel.text = user.email
    }
}
