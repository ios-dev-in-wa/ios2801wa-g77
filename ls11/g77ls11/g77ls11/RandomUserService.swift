//
//  RandomUserService.swift
//  g77ls11
//
//  Created by WA on 3/5/20.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct RandomUserService {

    let session = URLSession.shared

    func getUsers(completion: @escaping (([Result]) -> Void)) {
        guard let url = URL(string: "https://randomuser.me/api/?results=50") else { return }
        let task = session.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            switch response.statusCode {
            case 200:
                guard let data = data else { return }
                let decoder = JSONDecoder()
                do {
                    let response = try decoder.decode(UserResponse.self, from: data)
//                    print(response.results)
                    DispatchQueue.main.async {
                        completion(response.results)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            default: print(response.statusCode, "Unexpected")
            }
        }
        task.resume()
    }
}
