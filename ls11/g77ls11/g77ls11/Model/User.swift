//
//  User.swift
//  g77ls11
//
//  Created by WA on 3/5/20.
//  Copyright © 2020 WA. All rights reserved.
//

import Foundation

struct User {
    let imageName: String
    let name: String
}
